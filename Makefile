CC	= gcc
SRC_DIR	= src
SRC	= $(SRC_DIR)/misc.c $(SRC_DIR)/compress.c $(SRC_DIR)/io.c $(SRC_DIR)/main.c 
OPTS	= -O3 -Wall
BIN	= scrunch

main:
	$(CC) $(OPTS) $(SRC) -o $(BIN)
	strip $(BIN)

all: main

clean:
	rm -f $(BIN)
