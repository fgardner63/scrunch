/*

Copyright (c) 2016 Frank Gardner <finitefield256 AT gmail DOT com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


*/

#include "sc.h"

#define		CRLF_COMPRESS_BYTE		0x01
#define		LFLF_COMPRESS_BYTE		0x02

#define		COMPRESS_REP_MIN		3
#define		COMPRESS_REP_MAX		10

#define		MAX_DICT_TABLE_SIZE		10
		
static const s8 *L_t[MAX_DICT_TABLE_SIZE] =  
	{ NULL, NULL, NULL, "th", "he", "an","er", "in", "st", NULL};


u8 toggle_high_bit(u8 byte)
{
	u8 m = 1 << 7;
	return m ^ byte;
}

u8 sc_check_rep_bytes(u8 *ptr, u8 c)
{
	u8 *p = ptr + 1, n = 1;

	while(*p)
	{
		if(*p == c)
			n++;
		else 	break;

		if(n >= COMPRESS_REP_MAX)
			break;
		
		p++;
	}

	if(n < COMPRESS_REP_MIN)
		n = 0;

	return n;
	
}

u8 sc_lookup_dict_bytes(s8 *p)
{
	register u8 x = 0x08;
	s8 *fp = p+1;

	if(!p || *p == 0 || !fp)
		return 0;

	while(x >= 0x03)
	{
		if((strncmp(L_t[x], p, 2)) == 0)
			return x;
		--x;
	}

return 0;

}

u8 * sc_rev_lookup_dict_bytes(u8 b)
{
	if(b >= 0x03 && b <= 0x08)
		return (u8 *) L_t[b];

return NULL;
}

u8 * sc_compress(const s8 *p)
{	
	if(!p) return NULL;

	usize sz = strlen(p);
	u8 *nw_p = NULL, *nw_ptr, *old_nw_ptr, n = 0, *r_p, *r_ptr = NULL, db = 0;
	
	nw_ptr = old_nw_ptr = NULL;
	nw_p = (u8 *) calloc(sz + 1, 1);
	
	if(!nw_p || check_7bit_string((u8 *) p,sz)) return NULL;

	nw_ptr = nw_p;

	while(*p)
	{

		db = sc_lookup_dict_bytes((s8 *)p);

		if(db) 
		{
			old_nw_ptr = nw_ptr;
			*nw_ptr++ = db;
			p+=2;
			continue;
		}
		
		if(*p == 0x0A && (old_nw_ptr && *old_nw_ptr == 0x0D))
		{
			*old_nw_ptr = CRLF_COMPRESS_BYTE;
		}
		else
		if(*p == 0x0A && (old_nw_ptr && *old_nw_ptr == 0x0A))
		{
			*old_nw_ptr = LFLF_COMPRESS_BYTE;
		}
		else
		if(*p == 0x20 && (old_nw_ptr && isascii(*old_nw_ptr)))
                {
                        *old_nw_ptr = toggle_high_bit(*old_nw_ptr);
		 }
		else
		{
			old_nw_ptr = nw_ptr; 			
			*nw_ptr++ = *p;
		}
		p++;
	}

nw_ptr = nw_p;

r_p = calloc(sz + 1, 1);

if(!r_p)
{
	free(nw_p);
	return NULL;
}

	
r_ptr = r_p;

	while(*nw_ptr)
	{
		n = 0;
		 n = sc_check_rep_bytes((u8 *)nw_ptr, *nw_ptr); 
			
		*r_ptr++ = *nw_ptr;
	
		if(n >= COMPRESS_REP_MIN && n <= COMPRESS_REP_MAX)
		{
			*r_ptr++ = (u8) (n-1) + 0xF;
			nw_ptr+=n;
			continue;
		}

		nw_ptr++;

	}
	
free(nw_p);

return r_p;
}

// Proper bounds checking added during the decompression process to prevent a carefully generated file from triggering
// a heap overflow. Thanks to P. Ames for discovering the bug.

s8 * sc_decompress(const u8 *p, usize sz)
{
	register unsigned int i = 0, cnt = 0;
        s8 *nw_p = NULL, *nw_ptr = NULL;
	u8 b = 0, ob = 0, *r_ptr = NULL, *d_p = NULL , *r_p = NULL;
	const u8 *op = NULL;
	usize max_size = 0;

	max_size = (sz * 2);

        nw_p = (s8 *) calloc(max_size + 1, 1);

        if(!nw_p) return NULL;

	nw_ptr = nw_p;

	r_p = (u8 *) calloc(max_size + 1, 1);

	if(!r_p)
	{
		free(nw_p);	
		return NULL;
	}

	r_ptr = r_p;

	while(*p && cnt < max_size)
	{
		b = *p;
		if((b >= 0x10 && b <= 0x19) && ob && ob != LFLF_COMPRESS_BYTE && ob != CRLF_COMPRESS_BYTE && ob != 0x0a && ob != 0x0d)
                {	
			
                        for(i=(b - 0xF); i > 0; --i) 
			{
                                *r_ptr++ = ob;
				cnt++;
				
				if(cnt > max_size) 
					break;

				
			}

			ob = 0;
                }
		else
		{
			ob = b;	
			*r_ptr++ = b;
			cnt++;
		}

		p++;
		continue;
	}

	if(cnt > max_size)
	{
		fprintf(stderr,"Overflow detected. Aborting!\n");
		free(nw_p);
		free(r_p);
		return NULL;
	}

	cnt = 0;

	op = (const u8 *) r_p;

        while(*op && cnt < max_size)
	{
		d_p = sc_rev_lookup_dict_bytes(*op);

		if(d_p)
		{
			*nw_ptr++ = d_p[0];
			*nw_ptr++ = d_p[1];
			cnt+=2;
			op++;
			continue;			
		}

                if(!isascii(*op) && *op != LFLF_COMPRESS_BYTE && *op != CRLF_COMPRESS_BYTE)
                {
                        b = (s8) toggle_high_bit(*op);

        	        d_p = sc_rev_lookup_dict_bytes(b);
	
               		if(d_p)
                	{
                       		*nw_ptr++ = d_p[0];
                        	*nw_ptr++ = d_p[1];
				cnt+=2;
			}
			else	
                       	if(b == LFLF_COMPRESS_BYTE) 
			{	
				*nw_ptr++ = 0x0a;
				*nw_ptr++ = 0x0a;
				cnt+=2;
			}
			else
			if(b == CRLF_COMPRESS_BYTE)
			{
				*nw_ptr++ = 0x0d;
				*nw_ptr++ = 0x0a;
				cnt+=2;
			}
			else
			{
				*nw_ptr++ = b;
				cnt++;
			}

			*nw_ptr++ = 0x20;
			cnt++;
                }
		else
                if(*op == LFLF_COMPRESS_BYTE)
                 {
                        *nw_ptr++ = 0x0a;
                        *nw_ptr++ = 0x0a;
			cnt+=2;
		}
                else
                if(*op == CRLF_COMPRESS_BYTE)
                 {
                         *nw_ptr++ = 0x0d;
                         *nw_ptr++ = 0x0a;
                	 cnt+=2; 
		}
		else
		{
			*nw_ptr++ = (s8) *op;
			cnt++;
		}

		op++;
	}

	free(r_p);

        if(cnt >= max_size)
        {
                fprintf(stderr,"Overflow detected. Aborting.\n");
		free(nw_p);
	        return NULL;
        }

return nw_p;
}


u8 calc_perc_s(const usize old, const usize new)
{	
	return (unsigned char) ((((old - new)) * 100) / old);
}

