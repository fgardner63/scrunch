/*

Copyright (c) 2016 Frank Gardner <finitefield256 AT gmail DOT com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


*/

#include "sc.h"

u8 * sc_read_file(const s8 *filename)
{
	int fd = 0;
	struct stat st;
	u8 *data = NULL; 
	
	memset(&st, 0, sizeof(st));

	fd = open(filename, O_RDONLY | O_NONBLOCK);

	if(fd < 0)
	{
		fprintf(stderr,"error: cannot read file %s\n",filename);
		return NULL;
	}
	
	
	if((fstat(fd, &st))<0)
	{
		fprintf(stderr,"error: cannot stat the file %s\n",filename);
		close(fd);
		return NULL;
	}


	if((st.st_mode & S_IFMT) != S_IFREG)
	{
		fprintf(stderr,"error: %s is not a regular file\n",filename);
		close(fd);
		return NULL;
	}
		
	if((data = calloc(st.st_size + 1, 1)) == NULL)
	{
		fprintf(stderr,"error: cannot allocate enough memory to read the file %s\n",filename);
		close(fd);
		return NULL;
		
	}
	

	if((read(fd, data, st.st_size))<0)
	{
		fprintf(stderr,"error: error reading file %s\n",filename);
		free(data);
		close(fd);
		return NULL;		

	}

	close(fd);
		
	return data;
}


int sc_write_file(const s8 * filename, const u8 *data, const usize n, const int force)
{
	int fd , c, overwrite_file,mode;
	struct stat st;
	
	fd = c = overwrite_file = mode = 0;

	mode = O_WRONLY|O_CREAT|O_TRUNC;

	memset(&st, 0 ,sizeof(st));

	if(!(lstat(filename, &st)))
	{
		if(!force)
		{
			printf("The file %s already exist! Do you want to overwrite it [Y|N]? ", filename);
			c = tolower(getchar());
		}
		else c = 'y';

		if(c == 'y')
		{
			if((unlink(filename))<0)
			{
				fprintf(stderr,"error: cannot unlink file %s\n",filename);
				return -1;
			}
			overwrite_file = 1;
		}
		else
		{
			return -1;
		}

		

	}
	else
		mode |= O_EXCL;
		
	if((fd = open(filename,mode,S_IRUSR | S_IWUSR | S_IRGRP))<0)
	{
		fprintf(stderr,"error: cannot create file %s\n", filename);
		return -1;
		
	} 

	if((write(fd, data, n)) < n)
	{
		fprintf(stderr,"error: cannot write to file %s\n",filename);
		close(fd);
		return -1;

	}
	
	close(fd);
		
return 0;


}
