/*

Copyright (c) 2016 Frank Gardner <finitefield256 AT gmail DOT com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <getopt.h>

#define	 VERSION		"0.0.3a"

#ifndef         EOF
#define         EOF             -1
#endif


#define  SC_MODE_COMPRESS	0x01
#define	 SC_MODE_DECOMPRESS	0x02

typedef size_t usize;
typedef unsigned char u8;
typedef char s8;

u8 * sc_compress(const s8 *);
s8 * sc_decompress(const u8 *, usize);
u8 * sc_read_file(const s8 *);
u8   calc_perc_s(const usize, const usize);
int sc_write_file(const s8 *, const u8 *,  const usize, const int);
int check_7bit_string(const u8 *, usize);
