/*

Copyright (c) 2016 Frank Gardner <finitefield256 AT gmail DOT com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this 
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge, 
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.

*/


#include "sc.h"

#define FILENAME_BUF_SIZE		75

void sc_usage(const s8 *pname)
{
       printf("\nUsage: %s [options] filename\n",pname);
       printf("options:\n\t-c: compress a text file\n");
       printf("\t-d: decompress a sc compressed file\n");
       printf("\t-o <filename>: output file\n");
       printf("\t-v: verbose mode\n");
       printf("\t-f: force overwrite files without prompting\n\n");

exit(0);


}

int main(int argc, char *argv[])
{
	int c = 0, mode = 0, verbose =  0, force_overwrite = 0;
	s8 *fn, *outputfile, *fn_p = NULL, *ext = NULL;
	u8 *p = NULL, *data = NULL;
	usize x = 0, y = 0;
	char fn_buf[FILENAME_BUF_SIZE + 1];

	fn = outputfile = NULL;

	if(verbose || argc < 3)	
	{
		printf("sCrunch v%s\nDeveloped by: Frank Gardner (finitefield256@gmail.com)\n",VERSION);
		if(argc < 3)
			sc_usage(argv[0]);

	}

	while((c = getopt_long(argc, argv, "fvcdo:", NULL, NULL)) != EOF)
	{

		switch(c)
		{
			case 'f':
				force_overwrite = 1;
				break;

			case 'v':
				verbose = 1;
				break;

			case 'o':
                                outputfile = optarg;
				break;

			case 'c':
				mode += SC_MODE_COMPRESS;
				break;
			
			case 'd':
				mode += SC_MODE_DECOMPRESS;
				break;

			case '?':
				sc_usage(argv[0]);
			
			

		}
		continue;
			
	}


	fn = argv[optind];

	if(!mode || (mode != SC_MODE_COMPRESS && mode != SC_MODE_DECOMPRESS))
	{
		fprintf(stderr,"error: please use '-c' for compression or '-d' for decompression.\n");
		return -1; 
	}

	if(!fn)
	{
		fprintf(stderr,"No input filename specified.\n");
		return -1;
	}

	if(!outputfile)
	{
		memset(fn_buf, 0 , sizeof(fn_buf));
		
		if(mode == SC_MODE_COMPRESS)
		{
			 snprintf(fn_buf, FILENAME_BUF_SIZE - 10, "%s.sc", fn);
		}
		else
		{
			fn_p = strdup(fn);
			if((ext = strrchr(fn_p,'.')))
			{
				*ext = 0;
				snprintf(fn_buf, FILENAME_BUF_SIZE - 1, "%s.dc", fn_p);
				
			}
			else
				snprintf(fn_buf, FILENAME_BUF_SIZE - 1, "%s.dc", fn);

 		}

		outputfile = fn_buf;
		
	}
		
	if(!(data = sc_read_file(fn)))
	{
		return -1;
	}


	if(mode == SC_MODE_COMPRESS)
	{
		if(!(p = (u8 *) sc_compress((s8 *) data)))
                {
                        free(data);
                        return -1;
                }

		x = strlen((s8 *)data);
		y = strlen((s8*) p);
		
		if(verbose)
			printf("File %s has been compressed from %lu byte(s) -> %lu byte(s) (%d%% compressed)\n",fn,x,y, calc_perc_s(x,y));	

		if((sc_write_file(outputfile,p,y, force_overwrite))<0)
		{
			free(p);
			free(data);
			return -1;
			
		}
		if(verbose) 
			printf("Output saved to %s\n",outputfile);
	
		free(p);
	}
	else 
	{
		usize count = 0;

		count = strlen((s8 *) data);

		if(!(p = (u8 *) sc_decompress(data, count)))
                {
                        free(data);
                        return -1;
                }
		
		count = strlen((s8 *) p);

                if((sc_write_file(outputfile,p,count, force_overwrite))<0)
                {
                        free(p);
                        free(data);
                        return -1;

                }

                if(verbose)
			printf("Output saved to %s (%lu bytes)\n",outputfile, count);
			

	}

	free(data);

return 0;

}
